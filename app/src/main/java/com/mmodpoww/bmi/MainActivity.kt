package com.mmodpoww.bmi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import com.mmodpoww.bmi.databinding.ActivityMainBinding
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityMainBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener { calculate() }
    }

    private fun displayBmi(bmi: Double) {
        val formattedTip = NumberFormat.getCurrencyInstance().format(bmi)
        binding.bmi.text = getString(R.string.bmi, formattedTip)
    }
    private fun displayShape(shape:String) {
        binding.shape.text = shape
    }

    private fun calculate() {
        var weight = findViewById<EditText>(R.id.height)
        var height = findViewById<EditText>(R.id.weight)


        var weightValue = weight.text.toString().toDouble()
        var heightValue = height.text.toString().toDouble()
        var h1 = heightValue / 100
        var h2 = h1 * h1
        var bmi = weightValue / h2

        var shape =" "
            if (bmi < 18){
            shape = "UNDERWEIGHT"
        }else if(bmi <= 24.99 && bmi >= 18.5){
            shape = "NORMAL"
        }else if(bmi >= 25.0 && bmi <= 34.99){
            shape = "OVERWEIGHT"
        }else{
           shape = "OBESE"
        }
        displayBmi(bmi)
        displayShape(shape)
    }
}